import React, { useState, useEffect } from "react";
import "./App.css";
import axios from "axios"

function App() {
  const [state, setState] = useState({
    total: 0,
    transaction: [{ title: "", amount: 0}
    ]
  });
  async function getInitial() {
    try {
      const response = await axios.get("/api/finances");

      console.log(response);
var count = 0;
         response.data.data.map((money)=> {
           console.log(money)
          count += money.amount})
console.log(count)
      setState({ total: count, transaction: response.data.data });
    } catch (err) {
      console.log(err);
    }
  } 
   async function addData(file) {
    try {
      const response = await axios.post("/api/finances",file);
getInitial();
    } catch (err) {
      console.log(err);
    }
  }



  async function removeData(id) {
    console.log(id)
    try {
      const response = await axios.delete("/api/finances/"+id);
      console.log('incommmmmminggggggg')
getInitial();
      console.log(response);
    } catch (err) {
      console.log(err);
    }
  }


  useEffect(() => {
    getInitial();
  }, []);
  const removeIndex = (id) => {
    const x = state.transaction
    const deletion = x.splice(id, 1)
    const obj = { total: state.total - deletion[0].amount, transaction: x }
   
    console.log(deletion[0]._id)

    removeData(deletion[0]._id)
  }
  const addIndex = (e) => {
    e.preventDefault()

    const title = document.getElementById("titleInput").value
    const multiplier =  document.getElementById("expenseInput").checked ? -1:1
    const amount = document.getElementById("amountInput").value
    const obj = { text: title, amount: parseInt(amount)*multiplier }
const array = [...state.transaction, obj]
//addData({total:state.total + parseInt(amount)*multiplier, transaction:array})
addData(obj)
document.getElementById("form").reset()
}
  return (
    <div className="pageHolder">
      Welcome to your personal expense tracker!
      <div className="statHolder">
        <p className="stat">{state.total < 0 ? "-" : ""}${Math.abs(state.total)}</p>
        <p className="stat">{state.transaction.length} transactions</p>
      </div>
      <hr></hr>
      <div className="financesHolder">
        {state.transaction.map((data, index) => (
          <div className="transactionHolder">
            <button className="removeButton" onClick={() => removeIndex(index)}>
              X
            </button>
            <div className={"title_" +( data.amount >0? "income":"expense")+( data.amount ===0? "none":"")}>{data.text}</div>
            <div className={"title_" + (data.amount >0? "income":"expense")+( data.amount ===0? "none":"")}>
              {data.amount < 0 ? "-" : ""}${Math.abs(data.amount)}
            </div>
          </div>
        ))}
      </div>
        <form  id = "form" onSubmit={(e) => addIndex(e)}>
        <div className="transactionHolder">

        <input type="text" id = "titleInput" placeholder="Title of event" required></input>
        <div>
          <label>expense</label>
          <input type="radio" id="expenseInput" name="type" value="expense" required/><br></br>
          <label>income</label>
          <input type="radio" id="incomeInput" name="type" value="income" />
        </div>
        <input type="number" id = "amountInput" step = "1" placeholder="Amount" required></input>
        <input  className = "submit" type="submit" value="Submit"/>
        </div>

        </form>
    </div>
  );
}

export default App;
