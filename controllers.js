const Post = require("./model");

exports.getData = async (req, res, next) => {
  retrive = await Post.find();

  return res.status(200).json({
    data: retrive,
  });
};

exports.addData = async (req, res, next) => {
  try {
    const { text, amount } = req.body;

    const upload = await Post.create(req.body);
    return res.status(200).json({
      success: true,
      transaction: upload,
    });
  } catch (err) {
    console.log(err);
  }
};

exports.deleteData = async (req, res, next) => {
  var find
    try {
     find = await Post.findById(req.params.id);
  } catch (err) {
    if (err.name === "UnhandledPromiseRejectionWarning") {
      return res.status(404).json({ error: "that doesnt exist" });
    }
  }
  if (!find) {
    return res.status(404).json({ error: "that doesnt exist" });
  }

  await find.remove();
  return res.status(200).json({ transaction: "done" });
};
