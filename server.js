const mongoose = require ("mongoose")
const routes = require ("./routes")
const express = require ("express")

const app = express()
const port = process.env.port || 5000

app.use(express.json())

app.use("/api/finances", routes)
mongoose.connect("mongodb+srv://me:me@cluster0-ejvgl.mongodb.net/test?retryWrites=true&w=majority", {useUnifiedTopology: true, useNewUrlParser:true, useCreateIndex: true}, () =>console.log("connected"))


app.listen(port, () => console.log(`Example app listening at http://localhost:${port}`))