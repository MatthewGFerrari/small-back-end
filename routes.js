const express = require ("express")
const router = express.Router()
const {getData, addData, deleteData} = require ('./controllers')


router.route('/').get(getData).post(addData)

router.route('/:id').delete(deleteData)






module.exports = router

